const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// JSON Web Tokens

// Token Creation 
module.exports.createAccessToken = (user) => {
	
	/*

		{
		  _id: new ObjectId("634015b5b58bf311c42098b2"),
		  firstName: 'Jane',
		  lastName: 'Hufano',
		  email: 'janehufano@mail.com',
		  password: '$2b$10$r/06tdZSaigin.jAsaklAeIo8.BlriD/RzmMjTXv3RTAuL.EAhEVu',
		  isAdmin: false,
		  mobileNo: '09123456789',
		  enrollments: [],
		  __v: 0
		}

	*/

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}


	// Generate a token
		// jwt.sign(<payload>, secret)
	return jwt.sign(data, secret, {expiresIn: "1h"})

}
